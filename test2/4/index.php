<?php
ini_set('display_errors', 1);

define('DB_DSN', 'mysql:dbname=zeloras_access;host=localhost');
define('DB_USER', 'zeloras_access');
define('DB_PASSWORD', 'zeloras');

$pkey=openssl_pkey_new();
openssl_pkey_export($pkey, $privatekey);
$publickey = openssl_pkey_get_details($pkey);
$publickey = $publickey["key"];


$insertdata = (filter_input(INPUT_GET, 'action') === "retrieve");
$action = filter_input(INPUT_POST, 'action');
if (!empty($action)) {
    $email = filter_input(INPUT_POST, 'email');
    $phone = filter_input(INPUT_POST, 'phone');
    
    openssl_public_encrypt($email, $email_hash, $publickey);
    openssl_public_encrypt($phone, $phone_hash, $publickey);

    try {
        $dbo = new \PDO(DB_DSN, DB_USER, DB_PASSWORD);
    } catch (PDOException $e) {
        echo 'DB ERROR';
    }

    switch ($action) {
        case 'insert':
            $sql = "INSERT INTO `phonebase` (`phone`,`email`,`hash`, `pkey`) VALUES (:phone, :email, :hash, :pkey)";
            $query = $dbo->prepare($sql);
            $query->bindParam(':phone', $phone_hash, PDO::PARAM_LOB);
            $query->bindParam(':email', $email_hash, PDO::PARAM_LOB);
            $query->bindParam(':hash', md5($email), PDO::PARAM_STR);
            $query->bindParam(':pkey', base64_encode($privatekey));
            $query->execute();
            header('Location: ?action=retrieve');
        break;

        case 'get':
            $sql = "SELECT * FROM `phonebase` WHERE `hash` = ?";
            $query = $dbo->prepare($sql);
            $query->execute(array(md5($email)));
            while ($res = $query->fetch()) {
                openssl_private_decrypt($res['email'], $encrypted_mail, base64_decode($res['pkey']));
                if (md5($encrypted_mail) === md5($email)) {
                    openssl_private_decrypt($res['phone'], $encrypted_phone, base64_decode($res['pkey']));
                    mail($encrypted_mail, 'Retrieve phone', $encrypted_phone);
                }

            }
        break;
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet">
    </head>
    <body>
        <?php if (!$insertdata) { ?>
            <div class="menu">
                <a href="?action=retrieve" class="menu__link">Retrieve phone</a>
            </div>
            <form class="form" method="post">
                <fieldset class="form__fieldset">
                    <legend class="form__legend">Add your phone number</legend>
                    <div class="form-data">
                        <div class="form-data__row form-data-title">
                            Option 1. Add your phone number
                        </div>
                        <div class="form-data__row">
                            <label class="form-data__label" for="phone">Enter your PHONE:</label>
                            <input type="tel" name="phone" id="phone" class="form-data__input" required="required">
                        </div>
                        <div class="form-data__row">
                            <label class="form-data__label" for="email">Enter your e-mail*:</label>
                            <input type="email" name="email" id="email" class="form-data__input" required="required">
                        </div>
                        <div class="form-data__row form-data-description">
                            You will be able to retrieve you phone number later on using your e-mail.
                        </div>
                        <div class="form-data__row">
                            <input type="hidden" name="action" value="insert">
                            <button type="submit">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        <?php } else { ?>
            <div class="menu">
                <a href="?action=put" class="menu__link">Add phone</a>
            </div>
            <form class="form" method="post">
                <fieldset class="form__fieldset">
                    <legend class="form__legend">Retrieve your phone number</legend>
                    <div class="form-data">
                        <div class="form-data__row form-data-title">
                            Option 2. Retrieve your phone number
                        </div>
                        <div class="form-data__row">
                            <label class="form-data__label" for="email">Enter your e-mail*:</label>
                            <input type="email" name="email" id="email" class="form-data__input" required="required">
                        </div>
                        <div class="form-data__row form-data-description">
                            The phone number will be e-mailed to you
                        </div>
                        <div class="form-data__row">
                            <input type="hidden" name="action" value="get">
                            <button type="submit">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        <?php } ?>
    </body>
</html>

