<?php
$filename = filter_input(INPUT_GET, 'file');
$file_link = dirname(__FILE__).'/'.$filename;

if (file_exists($file_link)) {
    $ref = filter_input(INPUT_SERVER, 'HTTP_REFERER');
    preg_match_all("@^.*\/\/(.*?)\/@", $ref, $ref_name);

    if (!empty($ref_name[1][0])) {
        setcookie('referrer', $ref_name[1][0], time()+31536000, '/');
    }
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_link));
    readfile($file_link);
    exit;
} else {
    print 'File not exists';
}


