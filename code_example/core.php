<?php
namespace Core;
use Config;
use Components\{
    Auth, Banners, Buttons, Phoneauth,
    Socialvk, Counters, Logotype, Languages,
    Authcode, Stats, Phonecall
};


class Core {

    public $template = 'default',
            $config = [];

    private $components = [],
            $portal_id = 0,
            $modules = [],
            $connect = [],
            $errors = [],
            $inc_components = [],
            $preload_modules = [],
            $session_4login = "",
            $mac;

    public function __construct() {
        $session_4login = filter_input(INPUT_POST, 'session_4login');
        
        $this->portal_id = filter_input(INPUT_POST, 'point');
        $this->mac = filter_input(INPUT_POST, 'mac');
        $this->action = filter_input(INPUT_POST, 'action');
        $get_session = $_SESSION['session_4login'] ?? "";
        
        if (strlen($get_session) > 2) {
            $this->session_4login = filter_input(INPUT_SESSION, 'session_4login');
        } else {
            $this->session_4login = $_SESSION['session_4login'] = md5(time()."-".rand(0, rand(0, 6)));
        }

        /*
         * Следующие 2 массива указывают на подключаемые модули и подгружаемые
         */
        $this->modules = [
            'smsauth' => 'Phoneauth', // Авторизация по телефону
            'auth' => 'Auth', //Базовая авторизация, требуется для как таковой авторизации
            'vkauth' => 'Socialvk', // Авторизация через соц. сеть VK
            'button' => 'Buttons', // Обычные кнопки, как с авторизацией так и без
            'banners' => 'Banners', // Модуль с выводом баннера
            'counters' => 'Counters', // Вывод счетчиков
            'logotype' => 'Logotype', //Вывод логотипа
            'showcase' => 'Showcase', //Вывод витрины,
            'languages' => 'Languages', //Вывод мультиязычной поддержки
            'authcode' => 'Authcode', //Вывод авторизации по коду
            'stats' => 'Stats', //Статистика всего и вся,
            'phonecall' => 'Phonecall' //Авторизация по звонку на телефон
        ];
        
        $this->preload_modules = [
            'auth' => [ //Базовая авторизация пользователя
                'name' => 'auth',
                'rule' => '*'
            ],
            'button' => [ //Базовый вывод кнопок
                'name' => 'button',
                'rule' => 'auth'
            ],
            'logotype' => [ //Базовый вывод логотипа
                'name' => 'logotype',
                'rule' => '*'
            ],
            'stats' => [ //Статистика всего и вся
                'name' => 'stats',
                'rule' => '*'
            ]
        ];
        
        if ((int)$this->portal_id < 1) {
            $this->errors['core'][] = 'portal id not set';
        } else if (strlen($this->mac) < 10) {
            //$this->errors['core'][] = 'user mac empty';
        }
        
        if (sizeof($this->errors) > 0) {
            $this->showErrors($this->errors);
        }
    }
    
    /**
     * Инициализация всех скриптов
     * @return array
     */
    public function init(): array {
        $this->getConfig();
        return $this->modsConnect();
    }
    
    /**
     * Вывод ошибок и выход из скриптов
     * @param array $errors
     */
    public function showErrors(array $errors = []) {
        foreach ($errors as $module => $error) {
            if (is_array($error)) {
                foreach ($error as $er) {
                    print $module . ": " . $er . "\n<br />\n<br />";
                }
            } else {
                print $module . ": " . $error . "\n<br />\n<br />";
            }
        }
        exit();
    }
    
    /**
     * Получаем основные данные по конфигу из базы
     * @return array
     */
    private function getConfig(): array {
        
        $config = new \Config($this->portal_id, $this->preload_modules, $this->session_4login, $this->mac);
        $components = $config->getComponents();
        $connects = $config->getConnects();
        $getConfig = $config->getConfig();
        $getTemplate = $config->getTemplate();
        if (sizeof($config->errors) > 0) {
            return $this->showErrors($config->errors);
        }
        
        $getConfig['config_clone'] = $config;
        $this->components = $components;
        $this->connect = $connects;
        $this->config = $getConfig;
        $this->template = $getTemplate;

        return $this->components;
    }
    
    /**
     * Подключаем компоненты
     * @return array
     */
    private function modsConnect(): array {
        $modules_list = [];

        foreach ($this->modules as $key => $mod) {
            if (!in_array($key, $this->components)) {
                unset($this->modules[$key]);
            }
        }

        foreach ($this->modules as $name => $mod) {
            $modName = '\\'.$mod;
            $modName = new $modName($this->portal_id, $this->connect, $this->mac, $this->action, $this->config);
            $modInit = $modName->init();
            
            if (isset($modInit['configure_replace']) && sizeof($modInit['configure_replace']) > 0) {
                $this->config = $modInit['configure_replace'];
            }

            if (isset($modName->errors) && sizeof($modName->errors) > 0) {
                return $this->showErrors($modName->errors);
            }
            $this->inc_components[$name] = $modules_list[$name]['component'] = $modName;
            $modules_list[$name]['main'] = $this->getTemplateData($mod, $modInit);
        }

        return $modules_list;
    }
    
    /**
     * Подключаем шаблон компонента и выводим все в массив
     * @param string $component component name
     * @param array $return
     * @return array
     */
    private function getTemplateData(string $component = '', array $return = []): array {
        $component = strtolower($component);
        $currentDirTemplate_system = dirname(__FILE__).'/com/templates/'.$component.'/';
        $currentDirTemplate = dirname(dirname(__FILE__)).'/portals/'.$this->template.'/components/'.strtolower($component).'/';
        $return['css'] = '';
        $return['js'] = '';
        $return['template'] = '';
        
        if (isset($return['data']) && sizeof($return['data']) > 0) {
            if (!isset($return['no_template']) || !$return['no_template']) {
                if (file_exists($currentDirTemplate)) {
                    $return['css'] = file_get_contents($currentDirTemplate.$component.'.css');
                    $return['js'] = file_get_contents($currentDirTemplate.$component.'.js');

                    if (isset($return['hasPositions']) && $return['hasPositions']) {
                        $keyList = array_keys($return['data']);
                        foreach ($keyList as $key) {
                            $template = $currentDirTemplate.'template_' . $key . '.php';

                            if (file_exists($template)) {
                                ob_start();
                                require $template;
                                $return[$key]['template'] = ob_get_contents();
                                ob_end_clean();
                            } else {
                                $this->errors['core'][] = "file: '" . $template . "' not exist";
                            }
                        }

                    } else {
                        $template = $currentDirTemplate.'template.php';

                        if (file_exists($template)) {
                            ob_start();
                            require $template;
                            $return['template'] = ob_get_contents();
                            ob_end_clean();
                        } else {
                            $this->errors['core'][] = "file: '" . $template . "' not exist";
                        }
                    }
                } else {
                    $this->errors['core'][] = "Template: '".$this->template."' for component: '" . $component . "' not found";
                }

                if (sizeof($this->errors) > 0) {
                    $this->showErrors($this->errors);
                }
            }
        }
        
        return $return;
    }
    
    /**
     * Подтверждение соглашении о разглашении данных
     * @return boolean
     */
    private function hasAccept(): bool {
        $session = filter_input(INPUT_POST, 'session_4login');
        $accept = filter_input(INPUT_POST, 'termosuse');

        if ($session === $this->session_4login && mb_strlen($this->session_4login) > 9 && $accept) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Перенаправляем юзер запросы на компоненты
     */
    public function route() {
        $error_list = [];
        $route = filter_input(INPUT_POST, 'route') ?? "";
        $redirect_url = filter_input(INPUT_POST, 'redirect_url');
        
        if (strlen($redirect_url) > 1) {
            $this->config['redirect_portal_url'] = $redirect_url;
        }

        switch ($route) {
            case 'auth':
                if ($this->hasAccept()) {
                    $worker['auth'] = $this->inc_components['auth']->getAuth($this->config);
                } else {
                    $error_list['auth'] = "No accept kgb terms";
                }
                
            break;
        
            case 'feedbackpage':
                
            break;
        
            case 'feedback':
                
            break;
        
            default:

            break;
        }
        
        if (isset($worker) && sizeof($worker) > 0) {
            foreach ($worker as $wk) {
                if (isset($wk->errors) && sizeof($wk->errors) > 0) {
                    foreach ($wk->errors as $wkey => $wkval) {
                        $error_list[$wkey] = $wkval;
                    }
                }
            }

            if (sizeof($error_list) > 0) {
                return $this->showErrors($error_list);
            }
        }
    }
}