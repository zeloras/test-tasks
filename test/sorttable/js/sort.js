function removeclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        $attrib = $attrib.replace(new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i"), "")
        obj.setAttribute("class", $attrib);
    }
}

function hasclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        var reg = new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i");
        $attrib = reg.test($attrib);
        return $attrib;
    } else {
        return false;
    }
}

function addclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        $attrib = $attrib.replace(new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i"), "")
        $attrib = $attrib + " " + elem;
        obj.setAttribute("class", $attrib);
    } else {
        obj.setAttribute("class", elem);
    }
}

var Mime = function(item) {
    this.hasclass = function(elem){return hasclass(elem, item);};
    this.addclass = function(elem){return addclass(elem, item);};
    this.removeclass = function(elem){return removeclass(elem, item);};
};

var sort_table = new Object({
    config: {
        'sort_row': '.sort-table',
        'sort': '.sort-table th',
        'content': '.sort-table__content'
    },
    
    /**
     * init method
     */
    init: function () {
        var $self = this,
            $sort = document.querySelectorAll($self.config.sort);
    
        if ($sort.length > 0) {
            for (var $item of $sort) {
                $item.addEventListener('click', function () {
                    var $elements = Array.prototype.slice.call(document.querySelector($self.config.sort_row).children);
                    $self.sortInit(this, $elements.indexOf(this));
                }, false);
            }
        }
    },
    
    /**
     * Set class for item and call sort function
     * @param {object} item click element
     * 
     */
    sortInit: function(item, index) {
        var $self = this,
            $type = item.getAttribute('data-type'),
            $order = false;
        item = new Mime(item);

        if (item.hasclass('orderby_desc')) {
            item.removeclass('orderby_desc');
            $order = true;
        } else {
            item.addclass('orderby_desc');
        }
        
        $self.sortTable($type, $order, index);
    },
    
    /**
     * Format date to timestamp
     * @param {string} $date unformat date
     * @returns {integer}
     * 
     */
    formatDate: function ($date) {
        var $split_date = $date.split('.'),
            $format_date = new Date($split_date[2] + '-' + $split_date[1] + '-' + $split_date[0]);
        
        return $format_date.getTime();
    },
    
    /**
     * Start sort table
     * @param {string} $type data-type on tag
     * @param {boolean} $order ASC or DESC
     * @param {integer} $coll column 
     */
    sortTable: function($type, $order, $coll) {
        var $self = this,
            $lines,
            $state = false,
            $switching = true,
            $i, $x, $y,
            $shouldSwitch = false;
            $table = document.querySelector($self.config.content);

        while ($switching) {
            $switching = false;
            $lines = $table.querySelectorAll("tr");
            for ($i = 0; $i < ($lines.length - 1); $i++) {
                $x = $lines[$i].querySelectorAll("td")[$coll].innerHTML;
                $y = $lines[$i + 1].querySelectorAll("td")[$coll].innerHTML;
                
                switch ($type) {
                    case 'text':
                        $state = ($order) ? $x.toLowerCase() < $y.toLowerCase() : $x.toLowerCase() > $y.toLowerCase();
                    break;
                    
                    case 'date':
                        $state = ($order) ? (Number($self.formatDate($x)) < Number($self.formatDate($y))) : (Number($self.formatDate($x)) > Number($self.formatDate($y)));
                    break;
                    
                    default:
                        $state = ($order) ? (Number($x) < Number($y)) : (Number($x) > Number($y));
                    break;
                }
                
                if ($state) {
                    $shouldSwitch= true;
                    break;
                }
            }
            if ($shouldSwitch) {
                if ($lines[$i + 1] !== undefined) {
                    $lines[$i].parentNode.insertBefore($lines[$i + 1], $lines[$i]);
                    $switching = true;
                }
            }
        }
    }
});

sort_table.init();