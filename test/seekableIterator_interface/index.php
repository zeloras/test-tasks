<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Mytest extends SplFileObject implements SeekableIterator {}

$file = new Mytest('seekableIterator');
$file->seek(0);
echo $file->current();

