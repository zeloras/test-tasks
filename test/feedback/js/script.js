function removeclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        $attrib = $attrib.replace(new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i"), "")
        obj.setAttribute("class", $attrib);
    }
}

function hasclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        var reg = new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i");
        $attrib = reg.test($attrib);
        return $attrib;
    } else {
        return false;
    }
}

function addclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        $attrib = $attrib.replace(new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i"), "")
        $attrib = $attrib + " " + elem;
        obj.setAttribute("class", $attrib);
    } else {
        obj.setAttribute("class", elem);
    }
}

var Mime = function(item) {
    this.hasclass = function(elem){return hasclass(elem, item);};
    this.addclass = function(elem){return addclass(elem, item);};
    this.removeclass = function(elem){return removeclass(elem, item);};
};

var feedback = new Object({
    config: {
        'form': '.feedback',
        'error_class': 'errorField',
        'user_name': '#feedback__name',
        'user_email': '#feedback__email',
        'user_text': '#feedback__text',
        'field_rules': {
            'name': 'required',
            'text': 'required',
            'email': 'email'
        }
    },

    /**
     * Init binds for feedback form
     */
    init: function () {
        var $self = this,
            $current_form = document.querySelector($self.config.form);
        
        if ($current_form.length > 0) {
            $current_form.addEventListener('submit', function (e) {
                e.preventDefault();
                return $self.checkform(this);
            }, false);
        }
    },
    
    /**
     * Check submit form, validation
     */
    checkform: function () {
        var $self = this,
            $submit_cnt = 0,
            $fields = {
                'name': document.querySelector($self.config.user_name),
                'email': document.querySelector($self.config.user_email),
                'text': document.querySelector($self.config.user_text)
            };
            
        for (var $field in $fields) {
            switch ($self.config.field_rules[$field]) {
                case 'email':
                    $self.errorField(true, $fields[$field]);
                    var $re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if ($re.test($fields[$field].value)) {
                        $submit_cnt++;
                    } else {
                        $self.errorField(false, $fields[$field]);
                    }
                break;

                case 'required':
                    $self.errorField(true, $fields[$field]);
                    if ($fields[$field].value.length > 1) {
                        $submit_cnt++;
                    } else {
                        $self.errorField(true, $fields[$field]);
                    }
                break;

                default:
                    $submit_cnt++;
                break;
            }
        }

        if ($submit_cnt === Object.keys($fields).length) {
            $self.sendForm();
        }
    },
    
    /**
     * Ajax Send form method
     */
    sendForm: function () {
        var $self = this,
            $form = document.querySelector($self.config.form),
            $args = new FormData($form),
            $ajax = new XMLHttpRequest();
        
        $ajax.open('POST', $form.getAttribute('action'), false);
        $ajax.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        $ajax.send($args);
        
        if ($ajax.status === 200) {
            if ($ajax.responseText.length > 1) {
                if ($ajax.responseText === "Ok") {
                    alert('Feedback sent');
                }
            }
        }
    },
    
    /**
     * Add or remove error class from element
     * @param {boolean} status
     * @param {object} element
     * 
     */
    errorField: function (status, element) {
        console.log(element, status);
        var $self = this,
            element = new Mime(element);
        
        if (status) {
            element.removeclass($self.config.error_class);
        } else {
            element.addclass($self.config.error_class);
        }
    }
});

feedback.init();