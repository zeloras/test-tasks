<?php
if (sizeof($_POST) > 1) {
    $ajax = filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH');
    if ($ajax !== NULL && strtolower($ajax) === 'xmlhttprequest') {
        define('DB_DSN', 'mysql:dbname=zeloras_access;host=localhost');
        define('DB_USER', 'zeloras_access');
        define('DB_PASSWORD', 'zeloras');

        try {
            $dbo = new \PDO(DB_DSN, DB_USER, DB_PASSWORD);
        } catch (PDOException $e) {
            echo 'DB ERROR';
        }
        
        $sql = "INSERT INTO `feedback` (`name`,`email`,`text`,`created_at`) VALUES (:username, :useremail, :usertext, :created)";
        $query = $dbo->prepare($sql);

        $query->bindParam(':username', filter_input(INPUT_POST, 'username'), PDO::PARAM_STR);
        $query->bindParam(':useremail', filter_input(INPUT_POST, 'useremail'), PDO::PARAM_STR);
        $query->bindParam(':usertext', filter_input(INPUT_POST, 'usertext'), PDO::PARAM_STR);
        $query->bindParam(':created', date('Y-m-d H:i:s'), PDO::PARAM_STR);
        $query->execute();
        
        exit("Ok");
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Feedback</title>
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <form class="feedback" method="post" action="/test/feedback/">
            <fieldset class="feedback__fieldset">
                <div class="feedback__row">
                    <label for="feedback__name" class="feedback__label">Имя</label>
                    <input id="feedback__name" type="text" required="required" name="username" class="feedback__input">
                </div>
                <div class="feedback__row">
                    <label for="feedback__email" class="feedback__label">Email</label>
                    <input id="feedback__email" type="email" required="required" name="useremail" class="feedback__input">
                </div>
                <div class="feedback__row">
                    <label for="feedback__text" class="feedback__label">Текст</label>
                    <textarea id="feedback__text" class="feedback__textarea" name="usertext" required="required"></textarea>
                </div>
                <div class="feedback__row">
                    <button class="feedback__button" type="submit">Отправить</button>
                </div>
            </fieldset>
        </form>
        <script src="js/script.js"></script>
    </body>
</html>
